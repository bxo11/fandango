""" In this approach we use 3 separate lists for args, kwargs and results """

import fandango

def test_avg():
    """
    Returns the average value of the sequence.
    """
    arg_list = (((1,2,3,4),),)
    kwarg_list = ({},)
    result_list = (2.5,)
    for i in range(len(arg_list)):
        args = arg_list[i]
        kwargs = kwarg_list[i]
        result = result_list[i]
        assert fandango.functional.avg(*args, **kwargs) == result

def test_floor():
    """  """
    arg_list = ((2.3,), (),)
    kwarg_list = ({}, {"x": 2.3, "unit": 1.1},)
    result_list = (2, 2.2,)
    for i in range(len(arg_list)):
        args = arg_list[i]
        kwargs = kwarg_list[i]
        result = result_list[i]
        assert fandango.functional.floor(*args, **kwargs) == result

