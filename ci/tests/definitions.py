"""
1) TEST DEFINITION STRUCTURE

    tests = {
        'fandango.module.<function.|class.submethod|class::static_method>': { #fully qualified name of the method or function to be tested
            'case1': { #individual test suffix (see example below)
                'docs': "explain the test case",
                'params': ( #the same test will be run for each tuple in params
                    # args - arguments to pass to the method/function
                    # kwargs - named arguments to pass to the method/function
                    # init_args - arguments to pass to instance/class __init__ when being tested
                    # init_args - named arguments to pass to instance/class __init__ when being tested
                    # result - result for each test params (have to be the same type of what function that is tested returns)
                    (args, kwargs, init_args, init_kwargs, result),
                    (args, kwargs, init_args, init_kwargs, result),
                    ...
                },
            '': { #if this is empty string, we dont use suffix for test name (see example)
                    ...
                }
            }
    }

2) TEST DEFINITION EXAMPLE
    a) This definition:
        tests = {
                'fandango.functional.floor': {
                    '': {
                        'docs': 'This is my test docs'
                        'params': (
                            ((2.3,), {}, 2),
                            ((), {"x": 2.3, "unit": 1.1}, 2.2),
                        )
                    }
                }
        }

    will generate test named test_floor inside file /test/test_functional.py .
    This test will be runned two times, once with args (2.3,) asserting result 2,
    second time with kwargs a2d(x=2.3, unit=1.1) asserting result 2.2.
    The test itself will be documented with 'This is my test docs'

    b) This definition:
        tests = {
                'fandango.functional.floor': {
                'suffix': {
                    'docs': 'This is my test documentation'
                    'params': (
                        ((2.3,), {}, 2),
                        ((), {"x": 2.3, "unit": 1.1}, 2.2),
                    )
                }
        }

    will give the same, except the name of the test will be test_floor_suffix.

3) TESTS TO BE SKIPPED

skip_tests = [ 'module.function' ] # methods/classes to not test
"""

def a2d(**kwargs):
    return kwargs
    
skip_tests =  [
 'fandango.objects.ReleaseNumber::__str__',
]

#TODO: redefine test definitions according to the format defined above
tests = {
    'fandango.functional.avg': {
        '': {
                'docs': 'Returns the average value of the sequence.',
                'params': (
                    (((1, 2, 3, 4),), {}, (), {}, 2.5),
                ),
            }
        },
    # This is the same test, but for different arguments. Can we do this like that? First test will have only args, second one only kwargs. No need for additional test name.
    'fandango.functional.floor': {
        '': {
            'docs': 'Returns the floor value of the number.',
            'params': (
                ((2.3,), {}, (), {}, 2),
                ((), {"x": 2.3, "unit": 1.1}, (), {}, 2.2),
            )
        }
    }
}