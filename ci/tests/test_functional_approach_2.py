""" In this approach we use one list with tuples (args, kwargs, results)

This would require a slight change in the definitions:

    tests = {
        'fandango.module.function': {
            'case1': {
                'doc': "explain the test case",
                'params': (                                             #params passed to test
                    (args, kwargs, result, init_kwargs, init_args),     #subsequent test cases, for each one assert will be called
                    ...
                )
    }
"""



import fandango

def test_avg():
    """
    Returns the average value of the sequence.
    """
    params = [
        (((1, 2, 3, 4),), {}, 2.5),
    ]
    for args, kwargs, result in params:
        assert fandango.functional.avg(*args, **kwargs) == result

def test_floor():
    """  """
    params = [
        ((2.3,), {}, 2),
        ((), {"x": 2.3, "unit": 1.1}, 2.2),
    ]
    for args, kwargs, result in params:
        assert fandango.functional.floor(*args, **kwargs) == result
